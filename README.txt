The module allows you to show node author information in a custom format.
As it uses the cck userreference field, you can easily change the displayed author and keep the real author information in the uid field of the node table. This is useful if you work with freelance authors/journo's or if somebody else handles the actual content input/upload.

To use the module:

1. Enable the module 
2. Go to node type settings page Administer -> Content Management -> Content types. Select the node type you want to modify 
3. Add a userreference cck field to a node type. Select display template "Author field template"
4. Open the page with content type settings.
5. In a fieldset "Author information" for a field "Author field" select the userreference field you want to use
6. In "Author information template" add php code which returns formatted string. You can use $n (node object) and $u (user object) variables here. 
For example:
<?php
$name = (isset($u->profile_last_name) || isset($u->profile_first_name)) ?  $u->profile_first_name . ' ' . $u->profile_last_name : $u->name;
$user_link =  l($name, 'author/' . $u->uid);
print format_date($n->created, 'small') .' / <em>' . $u->profile_company_name . '</em> / '. $user_link;
?>
7. Save your settings.  